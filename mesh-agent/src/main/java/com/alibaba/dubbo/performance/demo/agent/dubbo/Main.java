//package com.alibaba.dubbo.performance.demo.agent.dubbo;
//
//import com.alibaba.dubbo.performance.demo.agent.dubbo.model.JsonUtils;
//import com.alibaba.dubbo.performance.demo.agent.dubbo.model.RpcInvocation;
//
//import java.io.*;
//import java.util.Arrays;
//
//public class Main {
//    public static void main(String[] args) throws Exception {
//        RpcInvocation invocation = new RpcInvocation();
//        invocation.setMethodName("hash");
//        invocation.setAttachment("version", "2.6.1");
//        invocation.setAttachment("path", "com.alibaba.dubbo.performance.demo.provider.IHelloService");
//        invocation.setParameterTypes("Ljava/lang/String;");    // Dubbo内部用"Ljava/lang/String"来表示参数类型是String
//
//        ByteArrayOutputStream out = new ByteArrayOutputStream();
//        PrintWriter writer = new PrintWriter(new OutputStreamWriter(out));
//        JsonUtils.writeObject("hello world", writer);
//        invocation.setArguments(out.toByteArray());
//
//        ByteArrayOutputStream bos = new ByteArrayOutputStream();
//
//        encodeRequestData(bos, invocation);
//
//
//        System.out.println(Arrays.toString(bos.toByteArray()));
//        String s  = new String(bos.toByteArray());
//        System.out.println(s.length());
//        System.out.println(s);
//    }
//
//    public static void encodeRequestData(OutputStream out, RpcInvocation inv) throws Exception {
//        PrintWriter writer = new PrintWriter(new OutputStreamWriter(out));
//
//        JsonUtils.writeObject(inv.getAttachment("dubbo", "2.0.1"), writer);
//        JsonUtils.writeObject(inv.getAttachment("path"), writer);
//        JsonUtils.writeObject(inv.getAttachment("version"), writer);
//        JsonUtils.writeObject(inv.getMethodName(), writer);
//        JsonUtils.writeObject(inv.getParameterTypes(), writer);
//
//        JsonUtils.writeBytes(inv.getArguments(), writer);
//        JsonUtils.writeObject(inv.getAttachments(), writer);
//    }
//}
